package com.pragma.hexagonal_arquitecture.user.application.adapters;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserPostService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UserPostAdapterTest {
    @Mock
    private IUserPostService iUserPostService;

    @InjectMocks
    private UserPostAdapter userPostAdapter;

    private UserDTO userDTO;
    private UserEntity userEntity;
    private static final Long USER_ID_TEST = 1L;
    private static final String USER_FIRSTNAME_TEST = "Darwin";
    private static final String USER_LASTNAME_TEST = "Grisales";
    private static final String USER_BIRTH_CITY_TEST = "Manizales";
    private static final String USER_DNI_TYPE_TEST = "Cedula";
    private static final String USER_DNI_TEST = "1234";
    private static final int USER_AGE_TEST = 19;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        userDTO = new UserDTO();
        userDTO.setId(USER_ID_TEST);
        userDTO.setFirstName(USER_FIRSTNAME_TEST);
        userDTO.setLastName(USER_LASTNAME_TEST);
        userDTO.setBirthCity(USER_BIRTH_CITY_TEST);
        userDTO.setDniType(USER_DNI_TYPE_TEST);
        userDTO.setDni(USER_DNI_TEST);
        userDTO.setAge(USER_AGE_TEST);

        userEntity = new UserEntity();
        userEntity.setId(USER_ID_TEST);
        userEntity.setFirstName(USER_FIRSTNAME_TEST);
        userEntity.setLastName(USER_LASTNAME_TEST);
        userEntity.setBirthCity(USER_BIRTH_CITY_TEST);
        userEntity.setDniType(USER_DNI_TYPE_TEST);
        userEntity.setDni(USER_DNI_TEST);
        userEntity.setAge(USER_AGE_TEST);
    }

    @Test
    void saveUser() {
        when(iUserPostService.save(any(UserEntity.class)))
                .thenReturn(userEntity);
        assertNotNull(userPostAdapter.saveUser(userDTO));
    }
}