package com.pragma.hexagonal_arquitecture.user.application.adapters;

import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserGetService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class UserGetAdapterTest {
    @Mock
    private IUserGetService iUserGetService;

    @InjectMocks
    private UserGetAdapter userGetAdapter;

    private UserEntity user;
    private static final Long USER_ID_TEST = 1L;
    private static final String USER_FIRSTNAME_TEST = "Darwin";
    private static final String USER_LASTNAME_TEST = "Grisales";
    private static final String USER_BIRTH_CITY_TEST = "Manizales";
    private static final String USER_DNI_TYPE_TEST = "Cedula";
    private static final String USER_DNI_TEST = "1234";
    private static final int USER_AGE_TEST = 19;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        user = new UserEntity();
        user.setId(USER_ID_TEST);
        user.setFirstName(USER_FIRSTNAME_TEST);
        user.setLastName(USER_LASTNAME_TEST);
        user.setBirthCity(USER_BIRTH_CITY_TEST);
        user.setDniType(USER_DNI_TYPE_TEST);
        user.setDni(USER_DNI_TEST);
        user.setAge(USER_AGE_TEST);
    }

    @Test
    void findAllUsers() {
        when(iUserGetService.findAll())
                .thenReturn(Collections.singletonList(user));
        assertNotNull(userGetAdapter.findAllUsers());
    }

    @Test
    void findUserById() {
        when(iUserGetService.findById(USER_ID_TEST))
                .thenReturn(user);
        assertNotNull(userGetAdapter.findUserById(USER_ID_TEST));
    }

    @Test
    void findUserByNullIdException() {
        try {
            userGetAdapter.findUserById(null);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void findUserByDni() {
        when(iUserGetService.findByDniTypeAndDni(USER_DNI_TYPE_TEST, USER_DNI_TEST))
                .thenReturn(user);
        assertNotNull(userGetAdapter.findUserByDni(USER_DNI_TYPE_TEST, USER_DNI_TEST));
    }

    @Test
    void findUserByNullDniTypeException() {
        try {
            userGetAdapter.findUserByDni(null, USER_DNI_TEST);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void findUserByEmptyDniException() {
        try {
            userGetAdapter.findUserByDni(USER_DNI_TYPE_TEST, "");
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void findUserByAge() {
        when(iUserGetService.findByAge(USER_AGE_TEST))
                .thenReturn(Collections.singletonList(user));
        assertNotNull(userGetAdapter.findUserByAge(USER_AGE_TEST));
    }
}