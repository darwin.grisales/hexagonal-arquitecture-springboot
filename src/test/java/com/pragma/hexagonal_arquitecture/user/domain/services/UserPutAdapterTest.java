package com.pragma.hexagonal_arquitecture.user.domain.services;

import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.repository.MySqlUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UserPutAdapterTest {
    @Mock
    private MySqlUserRepository userRepository;

    @InjectMocks
    private UserPutService userPutService;
    private UserEntity user;
    private static final Long USER_ID_TEST = 1L;
    private static final String USER_FIRSTNAME_TEST = "Darwin";
    private static final String USER_LASTNAME_TEST = "Grisales";
    private static final String USER_BIRTH_CITY_TEST = "Manizales";
    private static final String USER_DNI_TYPE_TEST = "Cedula";
    private static final String USER_DNI_TEST = "1234";
    private static final int USER_AGE_TEST = 19;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        user = new UserEntity();
        user.setId(USER_ID_TEST);
        user.setFirstName(USER_FIRSTNAME_TEST);
        user.setLastName(USER_LASTNAME_TEST);
        user.setBirthCity(USER_BIRTH_CITY_TEST);
        user.setDniType(USER_DNI_TYPE_TEST);
        user.setDni(USER_DNI_TEST);
        user.setAge(USER_AGE_TEST);
    }

    @Test
    void update() {
        when(userRepository.findById(USER_ID_TEST))
                .thenReturn(Optional.ofNullable(user));
        when(userRepository.save(any(UserEntity.class)))
                .thenReturn(user);
        assertNotNull(userPutService.update(USER_ID_TEST, user));
    }

    @Test
    void updateNotFoundException() {
        try {
            userPutService.update(USER_ID_TEST, user);
            fail();
        } catch (NotFoundException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(NotFoundException.class);
        }
    }
}