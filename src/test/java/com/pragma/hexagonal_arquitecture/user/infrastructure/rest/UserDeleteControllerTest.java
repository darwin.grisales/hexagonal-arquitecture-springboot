package com.pragma.hexagonal_arquitecture.user.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserDeleteAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserDeleteController.class)
class UserDeleteControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserDeleteAdapter iUserDeleteAdapter;

    private UserDTO user;
    private static final Long USER_ID_TEST = 1L;
    private static final String USER_FIRSTNAME_TEST = "Darwin";
    private static final String USER_LASTNAME_TEST = "Grisales";
    private static final String USER_BIRTH_CITY_TEST = "Manizales";
    private static final String USER_DNI_TYPE_TEST = "Cedula";
    private static final String USER_DNI_TEST = "1234";
    private static final int USER_AGE_TEST = 19;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        user = new UserDTO();
        user.setId(USER_ID_TEST);
        user.setFirstName(USER_FIRSTNAME_TEST);
        user.setLastName(USER_LASTNAME_TEST);
        user.setBirthCity(USER_BIRTH_CITY_TEST);
        user.setDniType(USER_DNI_TYPE_TEST);
        user.setDni(USER_DNI_TEST);
        user.setAge(USER_AGE_TEST);
    }

    @Test
    void deleteById() throws Exception {
        when(iUserDeleteAdapter.deleteUser(USER_ID_TEST))
                .thenReturn(user);
        mockMvc.perform(delete("/users/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value(USER_FIRSTNAME_TEST));
        verify(iUserDeleteAdapter).deleteUser(USER_ID_TEST);
    }

//    @Test
//    void deleteByIdNotFound() throws Exception {
//        when(iUserDeleteAdapter.deleteUser(USER_ID_TEST))
//                .thenReturn(null);
//        mockMvc.perform(delete("/users/1").contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
////        verify(iUserDeleteAdapter.deleteUser(USER_ID_TEST));
//    }
}