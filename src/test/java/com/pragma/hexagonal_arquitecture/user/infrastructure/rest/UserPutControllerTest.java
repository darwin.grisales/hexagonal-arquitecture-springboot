package com.pragma.hexagonal_arquitecture.user.infrastructure.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserPutAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserPutController.class)
class UserPutControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserPutAdapter userPutAdapter;
    private UserDTO user;
    private static final Long USER_ID_TEST = 1L;
    private static final String USER_FIRSTNAME_TEST = "Darwin";
    private static final String USER_LASTNAME_TEST = "Grisales";
    private static final String USER_BIRTH_CITY_TEST = "Manizales";
    private static final String USER_DNI_TYPE_TEST = "Cedula";
    private static final String USER_DNI_TEST = "1234";
    private static final int USER_AGE_TEST = 19;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        user = new UserDTO();
        user.setId(USER_ID_TEST);
        user.setFirstName(USER_FIRSTNAME_TEST);
        user.setLastName(USER_LASTNAME_TEST);
        user.setBirthCity(USER_BIRTH_CITY_TEST);
        user.setDniType(USER_DNI_TYPE_TEST);
        user.setDni(USER_DNI_TEST);
        user.setAge(USER_AGE_TEST);
    }

    @Test
    void updateUser() throws Exception {
        when(userPutAdapter.updateUser(eq(USER_ID_TEST), any(UserDTO.class)))
                .thenReturn(user);
        mockMvc.perform(put("/users/{id}", USER_ID_TEST)
                .content(asJsonString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value(USER_FIRSTNAME_TEST));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}