package com.pragma.hexagonal_arquitecture.user.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserGetAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserGetController.class)
class UserGetControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IUserGetAdapter iUserGetAdapter;

    private UserDTO user;
    private static final Long USER_ID_TEST = 1L;
    private static final String USER_FIRSTNAME_TEST = "Darwin";
    private static final String USER_LASTNAME_TEST = "Grisales";
    private static final String USER_BIRTH_CITY_TEST = "Manizales";
    private static final String USER_DNI_TYPE_TEST = "Cedula";
    private static final String USER_DNI_TEST = "1234";
    private static final int USER_AGE_TEST = 19;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        user = new UserDTO();
        user.setId(USER_ID_TEST);
        user.setFirstName(USER_FIRSTNAME_TEST);
        user.setLastName(USER_LASTNAME_TEST);
        user.setBirthCity(USER_BIRTH_CITY_TEST);
        user.setDniType(USER_DNI_TYPE_TEST);
        user.setDni(USER_DNI_TEST);
        user.setAge(USER_AGE_TEST);
    }

    @Test
    void findAllUsers() throws Exception {
        when(iUserGetAdapter.findAllUsers())
                .thenReturn(Collections.singletonList(user));
        mockMvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].firstName").value(USER_FIRSTNAME_TEST));
        verify(iUserGetAdapter).findAllUsers();
    }

    @Test
    void findUserById() throws Exception {
        when(iUserGetAdapter.findUserById(USER_ID_TEST))
                .thenReturn(user);
        mockMvc.perform(get("/users/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value(USER_FIRSTNAME_TEST));
        verify(iUserGetAdapter).findUserById(USER_ID_TEST);
    }

    @Test
    void findByDniAndDniType() throws Exception {
        when(iUserGetAdapter.findUserByDni(USER_DNI_TYPE_TEST, USER_DNI_TEST))
                .thenReturn(user);
        mockMvc.perform(get("/users/document?type=Cedula&dni=1234").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.firstName").value(USER_FIRSTNAME_TEST));
        verify(iUserGetAdapter).findUserByDni(USER_DNI_TYPE_TEST, USER_DNI_TEST);
    }

    @Test
    void findByAge() throws Exception {
        when(iUserGetAdapter.findUserByAge(USER_AGE_TEST))
                .thenReturn(Collections.singletonList(user));
        mockMvc.perform(get("/users/age?age=19").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].firstName").value(USER_FIRSTNAME_TEST));
        verify(iUserGetAdapter).findUserByAge(USER_AGE_TEST);
    }
}