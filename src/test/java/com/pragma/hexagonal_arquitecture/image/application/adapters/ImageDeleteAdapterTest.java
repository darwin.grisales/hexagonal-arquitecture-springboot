package com.pragma.hexagonal_arquitecture.image.application.adapters;

import com.pragma.hexagonal_arquitecture.image.domain.ports.IImageDeleteService;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

class ImageDeleteAdapterTest {
    @Mock
    private IImageDeleteService imageDeleteService;

    @InjectMocks
    private ImageDeleteAdapter imageDeleteAdapter;
    private ImageEntity imageEntity;
    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "6554";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageEntity = new ImageEntity();
        imageEntity.setNameImage("Cat");
        imageEntity.setImageContentType("image/jpg");
        imageEntity.setImageEncodedBytes("encodedType");
        imageEntity.setDni(IMAGE_DNI_TEST);
        imageEntity.setId(IMAGE_ID_TEST);
    }

    @Test
    void deleteImage() {
        when(imageDeleteService.delete(IMAGE_ID_TEST))
                .thenReturn(imageEntity);
        assertNotNull(imageDeleteAdapter.deleteImage(IMAGE_ID_TEST));
    }

    @Test
    void deleteImageByNullIdException() {
        try {
            imageDeleteAdapter.deleteImage(null);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }
}