package com.pragma.hexagonal_arquitecture.image.application.adapters;

import com.pragma.hexagonal_arquitecture.image.domain.ports.IImagePutService;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ImagePutAdapterTest {
    @Mock
    private IImagePutService iImagePutService;

    @InjectMocks
    private ImagePutAdapter imagePutAdapter;

    private MockMultipartFile file;
    private MockMultipartFile emptyFile;
    private ImageEntity imageEntity;
    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "6554";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageEntity = new ImageEntity();
        imageEntity.setNameImage("Cat");
        imageEntity.setImageContentType("image/jpg");
        imageEntity.setImageEncodedBytes("encodedType");
        imageEntity.setDni(IMAGE_DNI_TEST);
        imageEntity.setId(IMAGE_ID_TEST);

        file = new MockMultipartFile(
                "file",
                "cat.jps",
                String.valueOf(MediaType.IMAGE_JPEG),
                "image/jpeg".getBytes()
        );
        emptyFile = new MockMultipartFile("file", (byte[]) null);
    }

    @Test
    void updateImage() throws IOException {
        when(iImagePutService.updateImage(IMAGE_ID_TEST, file, IMAGE_DNI_TEST))
                .thenReturn(imageEntity);
        assertNotNull(imagePutAdapter.updateImage(IMAGE_ID_TEST, file, IMAGE_DNI_TEST));
    }

    @Test
    void updateImageNullIdException() throws IOException {
        try {
            imagePutAdapter.updateImage(null, file, IMAGE_DNI_TEST);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void updateImageEmptyIdException() throws IOException {
        try {
            imagePutAdapter.updateImage("", file, IMAGE_DNI_TEST);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void updateImageEmptyFileException() throws IOException {
        try {
            imagePutAdapter.updateImage(IMAGE_ID_TEST, emptyFile, IMAGE_DNI_TEST);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void updateImageNullDniException() throws IOException {
        try {
            imagePutAdapter.updateImage(IMAGE_ID_TEST, file, null);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void updateImageEmptyDniException() throws IOException {
        try {
            imagePutAdapter.updateImage(IMAGE_ID_TEST, file, "");
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }
}