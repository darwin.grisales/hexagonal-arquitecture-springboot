package com.pragma.hexagonal_arquitecture.image.application.adapters;

import com.pragma.hexagonal_arquitecture.image.domain.ports.IImageGetService;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ImageGetAdapterTest {
    @Mock
    private IImageGetService iImageGetService;

    @InjectMocks
    private ImageGetAdapter imageGetAdapter;

    private ImageEntity imageEntity;
    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "6554";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageEntity = new ImageEntity();
        imageEntity.setNameImage("Cat");
        imageEntity.setImageContentType("image/jpg");
        imageEntity.setImageEncodedBytes("encodedType");
        imageEntity.setDni(IMAGE_DNI_TEST);
        imageEntity.setId(IMAGE_ID_TEST);
    }

    @Test
    void findAllImages() {
        when(iImageGetService.findAll())
                .thenReturn(Collections.singletonList(imageEntity));
        assertNotNull(imageGetAdapter.findAllImages());
    }

    @Test
    void findImageById() {
        when(iImageGetService.findById(IMAGE_ID_TEST))
                .thenReturn(imageEntity);
        assertNotNull(imageGetAdapter.findImageById(IMAGE_ID_TEST));
    }

    @Test
    void findImageByNullIdException() {
        try {
            imageGetAdapter.findImageById(null);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void findImageByEmptyIdException() {
        try {
            imageGetAdapter.findImageById("");
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void findAllImagesByDni() {
        when(iImageGetService.findAllByDni(IMAGE_DNI_TEST))
                .thenReturn(Collections.singletonList(imageEntity));
        assertNotNull(imageGetAdapter.findAllImagesByDni(IMAGE_DNI_TEST));
    }

    @Test
    void findImageByEmptyDniException() {
        try {
            imageGetAdapter.findAllImagesByDni("");
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }

    @Test
    void findImageByNullDniException() {
        try {
            imageGetAdapter.findAllImagesByDni(null);
            fail();
        } catch (BadRequestException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(BadRequestException.class);
        }
    }
}