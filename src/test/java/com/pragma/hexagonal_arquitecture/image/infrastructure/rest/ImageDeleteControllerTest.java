package com.pragma.hexagonal_arquitecture.image.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImageDeleteAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ImageDeleteController.class)
class ImageDeleteControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IImageDeleteAdapter iImageDeleteAdapter;

    private ImageDTO imageDTO;
    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "6554";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageDTO = new ImageDTO();
        imageDTO.setNameImage("Cat");
        imageDTO.setImageContentType("image/jpg");
        imageDTO.setImageEncodedBytes("encodedType");
        imageDTO.setDni(IMAGE_DNI_TEST);
        imageDTO.setId(IMAGE_ID_TEST);
    }

    @Test
    void deleteImage() throws Exception {
        when(iImageDeleteAdapter.deleteImage(IMAGE_ID_TEST))
                .thenReturn(imageDTO);
        mockMvc.perform(delete("/images/{id}", IMAGE_ID_TEST)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.imageContentType").value("image/jpg"))
                .andExpect(jsonPath("$.imageEncodedBytes").isNotEmpty());
    }
}