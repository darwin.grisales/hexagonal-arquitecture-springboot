package com.pragma.hexagonal_arquitecture.image.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImagePutAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import java.io.IOException;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebMvcTest(ImagePutController.class)
class ImagePutControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IImagePutAdapter iImagePutAdapter;

    private MockMultipartFile file;
    private ImageDTO imageDTO;
    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "6554";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageDTO = new ImageDTO();
        imageDTO.setNameImage("Cat");
        imageDTO.setImageContentType("image/jpg");
        imageDTO.setImageEncodedBytes("encodedType");
        imageDTO.setDni(IMAGE_DNI_TEST);
        imageDTO.setId(IMAGE_ID_TEST);

        file = new MockMultipartFile(
                "file",
                "cat.jps",
                String.valueOf(MediaType.IMAGE_JPEG),
                "image/jpeg".getBytes()
        );
    }

    @Test
    void updateImage() throws Exception {
        when(iImagePutAdapter.updateImage(IMAGE_ID_TEST, file, IMAGE_DNI_TEST))
                .thenReturn(imageDTO);
        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/images/123");
        builder.with(new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                request.setMethod("PUT");
                return request;
            }
        });
        mockMvc.perform(builder
                        .file(file)
                        .param("dni", IMAGE_DNI_TEST))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.imageContentType").value("image/jpg"))
                .andExpect(jsonPath("$.imageEncodedBytes").exists());
    }
}