package com.pragma.hexagonal_arquitecture.image.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImageGetAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ImageGetController.class)
class ImageGetControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IImageGetAdapter iImageGetAdapter;

    private ImageDTO imageDTO;
    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "6554";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageDTO = new ImageDTO();
        imageDTO.setNameImage("Cat");
        imageDTO.setImageContentType("image/jpg");
        imageDTO.setImageEncodedBytes("encodedType");
        imageDTO.setDni(IMAGE_DNI_TEST);
        imageDTO.setId(IMAGE_ID_TEST);
    }

    @Test
    void findAll() throws Exception {
        when(iImageGetAdapter.findAllImages())
                .thenReturn(Collections.singletonList(imageDTO));
        mockMvc.perform(get("/images")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].imageContentType").value("image/jpg"))
                .andExpect(jsonPath("$.[0].imageEncodedBytes").exists());
    }

    @Test
    void findById() throws Exception {
        when(iImageGetAdapter.findImageById(IMAGE_ID_TEST))
                .thenReturn(imageDTO);
        mockMvc.perform(get("/images/{id}", IMAGE_ID_TEST)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.imageContentType").value("image/jpg"))
                .andExpect(jsonPath("$.imageEncodedBytes").exists());
    }

    @Test
    void findByDni() throws Exception {
        when(iImageGetAdapter.findAllImagesByDni(IMAGE_DNI_TEST))
                .thenReturn(Collections.singletonList(imageDTO));
        mockMvc.perform(get("/images/dni?dni=6554")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].imageContentType").value("image/jpg"))
                .andExpect(jsonPath("$.[0].imageEncodedBytes").exists());
    }
}