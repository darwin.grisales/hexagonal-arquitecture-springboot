package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ImageGetServiceTest {
    @Mock
    private MongoImageRepository imageRepository;

    @InjectMocks
    private ImageGetService imageGetService;

    private ImageEntity imageEntity;

    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "6554";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageEntity = new ImageEntity();
        imageEntity.setNameImage("Cat");
        imageEntity.setImageContentType("image/jpg");
        imageEntity.setImageEncodedBytes("encodedType");
        imageEntity.setDni(IMAGE_DNI_TEST);
        imageEntity.setId(IMAGE_ID_TEST);
    }

    @Test
    void findAll() {
        when(imageRepository.findAll())
                .thenReturn(Collections.singletonList(imageEntity));
        assertNotNull(imageGetService.findAll());
    }

    @Test
    void findById() {
        when(imageRepository.findById(IMAGE_ID_TEST))
                .thenReturn(Optional.ofNullable(imageEntity));
        assertNotNull(imageGetService.findById(IMAGE_ID_TEST));
    }

    @Test
    void findByIdNotFoundException() {
        try {
            imageGetService.findById(IMAGE_ID_TEST);
            fail();
        } catch (NotFoundException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(NotFoundException.class);
        }
    }

    @Test
    void findAllByDni() {
        when(imageRepository.findByDni(IMAGE_DNI_TEST))
                .thenReturn(Collections.singletonList(imageEntity));
        assertNotNull(imageGetService.findAllByDni(IMAGE_DNI_TEST));
    }
}