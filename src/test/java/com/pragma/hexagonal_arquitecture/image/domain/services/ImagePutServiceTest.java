package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class ImagePutServiceTest {
    @Mock
    private MongoImageRepository imageRepository;

    @InjectMocks
    private ImagePutService imagePutService;

    private ImageEntity image;
    private MockMultipartFile file;

    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "1234654";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        image = new ImageEntity();
        image.setNameImage("Cat");
        image.setImageContentType("image/jpg");
        image.setImageEncodedBytes("encodedType");
        image.setDni(IMAGE_DNI_TEST);
        image.setId(IMAGE_ID_TEST);

        file = new MockMultipartFile(
                "file",
                "cat.jps",
                String.valueOf(MediaType.IMAGE_JPEG),
                "image/jpeg".getBytes()
        );
    }

    @Test
    void updateImage() throws IOException {
        when(imageRepository.findById(eq(IMAGE_ID_TEST)))
                .thenReturn(Optional.ofNullable(image));
        when(imageRepository.save(any(ImageEntity.class)))
                .thenReturn(image);
        assertNotNull(imagePutService.updateImage(IMAGE_ID_TEST, file, IMAGE_DNI_TEST));
    }

    @Test
    void updateImageNotFoundException() throws IOException {
        try {
            imagePutService.updateImage(IMAGE_ID_TEST, file, IMAGE_DNI_TEST);
            fail();
        } catch (NotFoundException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(NotFoundException.class);
        }
    }
}