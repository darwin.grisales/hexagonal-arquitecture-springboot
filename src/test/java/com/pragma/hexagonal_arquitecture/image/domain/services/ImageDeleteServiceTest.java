package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ImageDeleteServiceTest {
    @Mock
    private MongoImageRepository imageRepository;

    @InjectMocks
    private ImageDeleteService imageDeleteService;

    private ImageEntity imageEntity;
    private static final String IMAGE_ID_TEST = "123";
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageEntity = new ImageEntity();
        imageEntity.setNameImage("Cat");
        imageEntity.setImageContentType("image/jpg");
        imageEntity.setImageEncodedBytes("encodedType");
        imageEntity.setDni("1234");
        imageEntity.setId(IMAGE_ID_TEST);

    }

    @Test
    void delete() {
        when(imageRepository.findById(IMAGE_ID_TEST))
                .thenReturn(Optional.ofNullable(imageEntity));
        assertNotNull(imageDeleteService.delete(IMAGE_ID_TEST));
    }

    @Test
    void deleteByIdNotFoundException() {
        try {
            imageDeleteService.delete(IMAGE_ID_TEST);
            fail();
        } catch (NotFoundException exception) {
            assertThat(exception.getClass())
                    .isEqualTo(NotFoundException.class);
        }
    }
}