package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

class ImagePostServiceTest {
    @Mock
    private MongoImageRepository imageRepository;

    @InjectMocks
    private ImagePostService imagePostService;

    private ImageEntity image;
    private MockMultipartFile file;

    private static final String IMAGE_ID_TEST = "123";
    private static final String IMAGE_DNI_TEST = "1234654";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        image = new ImageEntity();
        image.setNameImage("Cat");
        image.setImageContentType("image/jpg");
        image.setImageEncodedBytes("encodedType");
        image.setDni(IMAGE_DNI_TEST);

        file = new MockMultipartFile(
                "file",
                "cat.jps",
                String.valueOf(MediaType.IMAGE_JPEG),
                "image/jpeg".getBytes()
        );
    }

    @Test
    void saveImage() throws IOException {
        when(imageRepository.save(any(ImageEntity.class)))
                .thenReturn(image);
        assertNotNull(imagePostService.saveImage(file, IMAGE_DNI_TEST));
    }
}