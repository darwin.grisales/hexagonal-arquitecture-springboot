package com.pragma.hexagonal_arquitecture.shared.application;

import org.modelmapper.ModelMapper;

public class BaseAdapter {
    protected ModelMapper modelMapper;

    public BaseAdapter() {
        this.modelMapper = new ModelMapper();
    }
}
