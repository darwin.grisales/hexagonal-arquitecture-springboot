package com.pragma.hexagonal_arquitecture.user.domain.ports;

import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;

public interface IUserPutService {
    UserEntity update(Long id, UserEntity userRequest);
}
