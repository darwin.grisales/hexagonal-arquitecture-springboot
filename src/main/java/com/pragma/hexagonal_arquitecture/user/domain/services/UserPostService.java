package com.pragma.hexagonal_arquitecture.user.domain.services;

import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.ConflictException;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserPostService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.repository.MySqlUserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserPostService extends BaseService implements IUserPostService {
    private final MySqlUserRepository userRepository;

    public UserPostService(MySqlUserRepository userRepository) {
        super();
        this.userRepository = userRepository;
    }

    @Override
    public UserEntity save(UserEntity userEntity) {
        UserEntity isUserExists = userRepository
                .findByDniTypeAndDni(userEntity.getDniType(), userEntity.getDni());
        if (isUserExists != null) {
            throw new ConflictException("User already exist in database");
        }
        return userRepository.save(userEntity);
    }
}
