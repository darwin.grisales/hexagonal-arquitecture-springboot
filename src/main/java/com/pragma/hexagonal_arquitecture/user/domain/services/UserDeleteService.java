package com.pragma.hexagonal_arquitecture.user.domain.services;

import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserDeleteService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.repository.MySqlUserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserDeleteService extends BaseService implements IUserDeleteService {
    private final MySqlUserRepository userRepository;

    public UserDeleteService(MySqlUserRepository userRepository) {
        super();
        this.userRepository = userRepository;
    }

    @Override
    public UserEntity deleteEntity(Long id) {
        UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(ERROR_MESSAGE_TEMPLATE, id)));
        userRepository.delete(user);
        return user;
    }
}
