package com.pragma.hexagonal_arquitecture.user.domain.services;

import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserPutService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.repository.MySqlUserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserPutService extends BaseService implements IUserPutService {
    private final MySqlUserRepository userRepository;

    public UserPutService(MySqlUserRepository userRepository) {
        super();
        this.userRepository = userRepository;
    }

    @Override
    public UserEntity update(Long id, UserEntity userRequest) {
        UserEntity userProcess = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(
                        String.format(ERROR_MESSAGE_TEMPLATE, id)));
        userProcess.setFirstName(userRequest.getFirstName());
        userProcess.setLastName(userRequest.getLastName());
        userProcess.setBirthCity(userRequest.getBirthCity());
        userProcess.setDniType(userRequest.getDniType());
        userProcess.setDni(userRequest.getDni());
        userProcess.setAge(userRequest.getAge());
        return userRepository.save(userProcess);
    }
}
