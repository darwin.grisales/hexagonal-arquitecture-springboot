package com.pragma.hexagonal_arquitecture.user.domain.ports;

import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;

import java.util.List;

public interface IUserGetService {
    List<UserEntity> findAll();
    UserEntity findById(Long id);
    UserEntity findByDniTypeAndDni(String dniType, String dni);
    List<UserEntity> findByAge(int age);
}
