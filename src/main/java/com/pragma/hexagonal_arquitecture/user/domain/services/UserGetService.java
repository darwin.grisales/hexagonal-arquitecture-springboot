package com.pragma.hexagonal_arquitecture.user.domain.services;

import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserGetService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.repository.MySqlUserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserGetService extends BaseService implements IUserGetService {
    private final MySqlUserRepository userRepository;

    public UserGetService(MySqlUserRepository userRepository) {
        super();
        this.userRepository = userRepository;
    }

    @Override
    public List<UserEntity> findAll() {
        return userRepository.findAll();
    }

    @Override
    public UserEntity findById(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(ERROR_MESSAGE_TEMPLATE, id)));
    }

    @Override
    public UserEntity findByDniTypeAndDni(String dniType, String dni) {
        UserEntity user = userRepository.findByDniTypeAndDni(dniType, dni);
        if (user == null) {
            throw new NotFoundException(
                    String.format("Not found with by DNI - %s and DNI type: %s", dni, dniType));
        }
        return user;
    }

    @Override
    public List<UserEntity> findByAge(int age) {
        List<UserEntity> userList = userRepository.findByAgeGreaterThanEqual(age);
        if (userList.isEmpty()) {
            throw new NotFoundException(String.format("Not found by age: %d", age));
        }
        return userList;
    }
}
