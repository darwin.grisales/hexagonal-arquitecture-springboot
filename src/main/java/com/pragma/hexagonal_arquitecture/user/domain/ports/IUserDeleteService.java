package com.pragma.hexagonal_arquitecture.user.domain.ports;

import com.pragma.hexagonal_arquitecture.user.domain.model.User;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;

public interface IUserDeleteService {
    UserEntity deleteEntity(Long id);
}
