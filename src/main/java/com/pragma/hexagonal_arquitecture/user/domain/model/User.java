package com.pragma.hexagonal_arquitecture.user.domain.model;

public class User {
    private Long id;
    private String firstName;
    private String lastName;
    private String dniType;
    private String dni;
    private String birthCity;
    private int age;

    public User(Long id, String firstName, String lastName, String dniType, String dni, String birthCity, int age) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dniType = dniType;
        this.dni = dni;
        this.birthCity = birthCity;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDniType() {
        return dniType;
    }

    public void setDniType(String dniType) {
        this.dniType = dniType;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
