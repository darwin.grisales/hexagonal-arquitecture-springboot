package com.pragma.hexagonal_arquitecture.user.domain.ports;

import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;

public interface IUserPostService {
    UserEntity save(UserEntity userEntity);
}
