package com.pragma.hexagonal_arquitecture.user.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserDeleteAdapter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "User")
public class UserDeleteController {
    private final IUserDeleteAdapter userDeleteAdapter;

    public UserDeleteController(IUserDeleteAdapter userDeleteAdapter) {
        this.userDeleteAdapter = userDeleteAdapter;
    }

    @Operation(summary = "Delete a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User deleted succefully",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = UserDTO.class))),
            @ApiResponse(responseCode = "400", description = "Error to delete a user",
                content = @Content)
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<UserDTO> deleteById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok().body(userDeleteAdapter.deleteUser(id));
    }
}
