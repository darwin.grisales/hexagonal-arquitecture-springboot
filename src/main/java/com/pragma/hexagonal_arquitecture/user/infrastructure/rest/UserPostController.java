package com.pragma.hexagonal_arquitecture.user.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserPostAdapter;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserPostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "User")
public class UserPostController {
    private final IUserPostAdapter userPostAdapter;

    public UserPostController(IUserPostAdapter userPostAdapter) {
        this.userPostAdapter = userPostAdapter;
    }

    @Operation(summary = "Save a user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User saved in DB",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = UserDTO.class))),
            @ApiResponse(responseCode = "400", description = "Error to save user",
                content = @Content)
    })
    @PostMapping
    public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO userDTO) {
        return new ResponseEntity<>(userPostAdapter.saveUser(userDTO), HttpStatus.CREATED);
    }
}
