package com.pragma.hexagonal_arquitecture.user.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserGetAdapter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@Tag(name = "User")
public class UserGetController {
    private final IUserGetAdapter userGetAdapter;

    public UserGetController(IUserGetAdapter userGetAdapter) {
        this.userGetAdapter = userGetAdapter;
    }

    @Operation(summary = "Get a list of all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found list of users",
                content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation =UserDTO.class))}),
    })
    @GetMapping
    public ResponseEntity<List<UserDTO>> findAllUsers() {
        return ResponseEntity.ok().body(userGetAdapter.findAllUsers());
    }

    @Operation(summary = "Find a user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found user by id",
                content = { @Content(mediaType = "application/json",
                    schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad id supplied",
                content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<UserDTO> findUserById(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok()
                .body(userGetAdapter.findUserById(id));
    }

    @Operation(summary = "Find a user by dni")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found user by dni",
                    content = { @Content(mediaType = "application/json",
                        schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad params supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)
    })
    @GetMapping("/document")
    public ResponseEntity<UserDTO> findByDniAndDniType(@RequestParam(name = "dni") String dni,
                                                       @RequestParam(name = "type") String dniType) {
        return ResponseEntity.ok().body(userGetAdapter.findUserByDni(dniType, dni));
    }

    @Operation(summary = "Find a user by age")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found users by age",
                    content = { @Content(mediaType = "application/json",
                        schema = @Schema(implementation = UserDTO.class))}),
            @ApiResponse(responseCode = "400", description = "Bad age supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content)
    })
    @GetMapping("/age")
    public ResponseEntity<List<UserDTO>> findByAge(@RequestParam(name = "age") int age) {
        return ResponseEntity.ok().body(userGetAdapter.findUserByAge(age));
    }
}
