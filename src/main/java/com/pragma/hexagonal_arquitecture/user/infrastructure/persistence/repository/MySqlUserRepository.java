package com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.repository;

import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MySqlUserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByDniTypeAndDni(String dniType, String dni);
    List<UserEntity> findByAgeGreaterThanEqual(int age);
}
