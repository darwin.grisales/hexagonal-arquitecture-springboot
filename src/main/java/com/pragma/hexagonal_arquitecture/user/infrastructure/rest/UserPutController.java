package com.pragma.hexagonal_arquitecture.user.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserPutAdapter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Tag(name = "User")
public class UserPutController {
    private final IUserPutAdapter userPutAdapter;

    public UserPutController(IUserPutAdapter userPutAdapter) {
        this.userPutAdapter = userPutAdapter;
    }

    @Operation(summary = "Update user in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated user in database",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = UserDTO.class))),
            @ApiResponse(responseCode = "400", description = "Error updating the user",
                content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<UserDTO> updateUser(@PathVariable(name = "id") Long id,
                                              @RequestBody UserDTO userDTO) {
        return ResponseEntity.ok().body(userPutAdapter.updateUser(id, userDTO));
    }
}
