package com.pragma.hexagonal_arquitecture.user.application.interfaces;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;

public interface IUserPostAdapter {
    UserDTO saveUser(UserDTO userDTO);
}
