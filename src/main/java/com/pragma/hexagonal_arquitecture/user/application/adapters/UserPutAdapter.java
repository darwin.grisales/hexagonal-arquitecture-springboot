package com.pragma.hexagonal_arquitecture.user.application.adapters;

import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserPutAdapter;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserPutService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import org.springframework.stereotype.Service;

@Service
public class UserPutAdapter extends BaseAdapter implements IUserPutAdapter {
    private final IUserPutService userPutService;

    public UserPutAdapter(IUserPutService userPutService) {
        super();
        this.userPutService = userPutService;
    }

    @Override
    public UserDTO updateUser(Long id, UserDTO userRequest) {
        if (id == null) {
            throw new BadRequestException("Param can not be null");
        }
        UserEntity user = modelMapper.map(userRequest, UserEntity.class);
        UserEntity userResponse = userPutService.update(id, user);
        return modelMapper.map(userResponse, UserDTO.class);
    }
}
