package com.pragma.hexagonal_arquitecture.user.application.adapters;

import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserPostAdapter;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserPostService;
import com.pragma.hexagonal_arquitecture.user.infrastructure.persistence.entity.UserEntity;
import org.springframework.stereotype.Service;

@Service
public class UserPostAdapter extends BaseAdapter implements IUserPostAdapter {
    private final IUserPostService userPostService;

    public UserPostAdapter(IUserPostService userPostService) {
        super();
        this.userPostService = userPostService;
    }

    @Override
    public UserDTO saveUser(UserDTO userDTO) {
        UserEntity user = modelMapper.map(userDTO, UserEntity.class);
        return modelMapper.map(userPostService.save(user), UserDTO.class);
    }
}
