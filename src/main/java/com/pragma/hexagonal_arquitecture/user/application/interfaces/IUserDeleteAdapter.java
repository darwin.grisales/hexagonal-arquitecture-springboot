package com.pragma.hexagonal_arquitecture.user.application.interfaces;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;

public interface IUserDeleteAdapter {
    UserDTO deleteUser(Long id);
}
