package com.pragma.hexagonal_arquitecture.user.application.adapters;

import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserGetService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserGetAdapter extends BaseAdapter implements com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserGetAdapter {
    private final IUserGetService userGetService;

    public UserGetAdapter(IUserGetService userGetService) {
        super();
        this.userGetService = userGetService;
    }

    @Override
    public List<UserDTO> findAllUsers() {
        return userGetService.findAll()
                .stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO findUserById(Long id) {
        if (id == null) {
            throw new BadRequestException("Param can not be null");
        }
        return modelMapper.map(userGetService.findById(id), UserDTO.class);
    }

    @Override
    public UserDTO findUserByDni(String dniType, String dni) {
        if (dniType == null || dni == null ||
                dniType.equals("") || dni.equals("")) {
            throw new BadRequestException(
                    String.format("Params can not be null - Dni: %s and type: %s", dni, dniType));
        }
        return modelMapper.map(
                userGetService.findByDniTypeAndDni(dniType, dni), UserDTO.class);
    }

    @Override
    public List<UserDTO> findUserByAge(int age) {
        return userGetService.findByAge(age)
                .stream()
                .map(user -> modelMapper.map(user, UserDTO.class))
                .collect(Collectors.toList());
    }
}
