package com.pragma.hexagonal_arquitecture.user.application.interfaces;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;

import java.util.List;

public interface IUserGetAdapter {
    List<UserDTO> findAllUsers();
    UserDTO findUserById(Long id);
    UserDTO findUserByDni(String dniType, String dni);
    List<UserDTO> findUserByAge(int age);
}
