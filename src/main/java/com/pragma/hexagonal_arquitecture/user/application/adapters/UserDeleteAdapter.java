package com.pragma.hexagonal_arquitecture.user.application.adapters;

import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;
import com.pragma.hexagonal_arquitecture.user.application.interfaces.IUserDeleteAdapter;
import com.pragma.hexagonal_arquitecture.user.domain.ports.IUserDeleteService;
import org.springframework.stereotype.Service;

@Service
public class UserDeleteAdapter extends BaseAdapter implements IUserDeleteAdapter {
    private final IUserDeleteService userDeleteService;

    public UserDeleteAdapter(IUserDeleteService userDeleteService) {
        super();
        this.userDeleteService = userDeleteService;
    }

    @Override
    public UserDTO deleteUser(Long id) {
        if (id == null) {
            throw new BadRequestException("Param can not be null");
        }
        return modelMapper
                .map(userDeleteService.deleteEntity(id), UserDTO.class);
    }
}
