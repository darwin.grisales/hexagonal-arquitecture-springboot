package com.pragma.hexagonal_arquitecture.user.application.interfaces;

import com.pragma.hexagonal_arquitecture.user.application.dto.UserDTO;

public interface IUserPutAdapter {
    UserDTO updateUser(Long id, UserDTO userRequest);
}
