package com.pragma.hexagonal_arquitecture.image.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImagePutAdapter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/images")
@Tag(name = "Image")
public class ImagePutController {
    private final IImagePutAdapter imagePutAdapter;

    public ImagePutController(IImagePutAdapter imagePutAdapter) {
        this.imagePutAdapter = imagePutAdapter;
    }

    @Operation(summary = "Update a image in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image updated succefully",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ImageDTO.class))),
            @ApiResponse(responseCode = "404", description = "Image not found",
                content = @Content),
            @ApiResponse(responseCode = "400", description = "Bad request param",
                content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<ImageDTO> updateImage(@PathVariable(name = "id") String id,
                                      @RequestParam(name = "file") MultipartFile multipartFile,
                                      @RequestParam(name = "dni") String dni) throws IOException {
        return ResponseEntity.ok().body(imagePutAdapter.updateImage(id, multipartFile, dni));
    }
}
