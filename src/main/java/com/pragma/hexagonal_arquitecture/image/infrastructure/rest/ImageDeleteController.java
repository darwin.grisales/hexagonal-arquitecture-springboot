package com.pragma.hexagonal_arquitecture.image.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImageDeleteAdapter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/images")
@Tag(name = "Image")
public class ImageDeleteController {
    private final IImageDeleteAdapter iImageDeleteAdapter;

    public ImageDeleteController(IImageDeleteAdapter iImageDeleteAdapter) {
        this.iImageDeleteAdapter = iImageDeleteAdapter;
    }

    @Operation(summary = "Delete a image from database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Image deleted from database",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ImageDTO.class))),
            @ApiResponse(responseCode = "404", description = "Image not found in database",
                content = @Content),
            @ApiResponse(responseCode = "400", description = "Bad request param",
                content = @Content)
    })
    @DeleteMapping("/{id}")
    public ImageDTO deleteImage(@PathVariable(name = "id") String id) {
        return iImageDeleteAdapter.deleteImage(id);
    }
}
