package com.pragma.hexagonal_arquitecture.image.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImageGetAdapter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/images")
@Tag(name = "Image")
public class ImageGetController {
    private final IImageGetAdapter imageGetAdapter;

    public ImageGetController(IImageGetAdapter imageGetAdapter) {
        this.imageGetAdapter = imageGetAdapter;
    }

    @Operation(summary = "Get All images in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found all images on database",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ImageDTO.class))),
            @ApiResponse(responseCode = "404", description = "Not Images on database",
                content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<ImageDTO>> findAll() {
        return ResponseEntity.ok().body(imageGetAdapter.findAllImages());
    }

    @Operation(summary = "Get last image for user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the most recent image with id",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ImageDTO.class))),
            @ApiResponse(responseCode = "404", description = "Not found images with id",
                content = @Content)
    })
    @GetMapping("/{id}")
    public ResponseEntity<ImageDTO> findById(@PathVariable(name = "id") String id) {
        return ResponseEntity.ok().body(imageGetAdapter.findImageById(id));
    }

    @Operation(summary = "Get all images for One User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found all images for one user by id",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ImageDTO.class))),
            @ApiResponse(responseCode = "404", description = "Not found images by id",
                content = @Content)
    })
    @GetMapping("/dni")
    public ResponseEntity<List<ImageDTO>> findByDni(@RequestParam(name = "dni") String dni) {
        return ResponseEntity.ok().body(imageGetAdapter.findAllImagesByDni(dni));
    }
}
