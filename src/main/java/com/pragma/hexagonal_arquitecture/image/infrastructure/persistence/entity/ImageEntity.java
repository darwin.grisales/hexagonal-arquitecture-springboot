package com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "images")
@Data
public class ImageEntity {
    @Id
    private String id;
    private String imageContentType;
    private String nameImage;
    private String dni;
    private String imageEncodedBytes;
}
