package com.pragma.hexagonal_arquitecture.image.infrastructure.rest;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImagePostAdapter;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/images")
@Tag(name = "Image")
public class ImagePostController {
    private final IImagePostAdapter imagePostAdapter;

    public ImagePostController(IImagePostAdapter imagePostAdapter) {
        this.imagePostAdapter = imagePostAdapter;
    }

    @Operation(summary = "Save a image in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Saved image in database",
                content = @Content(mediaType = "application/json",
                        schema = @Schema(implementation = ImageDTO.class))),
            @ApiResponse(responseCode = "400", description = "Error to save an image",
                content = @Content)
    })
    @PostMapping
    public ResponseEntity<ImageDTO> saveImage(@RequestParam(name = "file") MultipartFile file,
                                    @RequestParam(name = "dni") String dni) throws IOException {
        return ResponseEntity.ok().body(imagePostAdapter.saveImage(file, dni));
    }
}
