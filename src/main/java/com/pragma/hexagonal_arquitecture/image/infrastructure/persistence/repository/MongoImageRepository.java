package com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoImageRepository
        extends MongoRepository<ImageEntity, String> {
    List<ImageEntity> findByDni(String dni);
}
