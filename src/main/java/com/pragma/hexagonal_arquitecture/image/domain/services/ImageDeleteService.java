package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.domain.ports.IImageDeleteService;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ImageDeleteService extends BaseService implements IImageDeleteService {
    private final MongoImageRepository imageRepository;

    public ImageDeleteService(MongoImageRepository imageRepository) {
        super();
        this.imageRepository = imageRepository;
    }

    @Override
    public ImageEntity delete(String id) {
        ImageEntity image = imageRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(
                        String.format(ERROR_MESSAGE_TEMPLATE, id)));
        imageRepository.delete(image);
        return image;
    }
}
