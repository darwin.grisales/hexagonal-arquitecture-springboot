package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.domain.model.Image;
import com.pragma.hexagonal_arquitecture.image.domain.ports.IImagePostService;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

@Service
public class ImagePostService extends BaseService implements IImagePostService {
    private final ModelMapper modelMapper;
    private final MongoImageRepository imageRepository;

    public ImagePostService(ModelMapper modelMapper, MongoImageRepository imageRepository) {
        super();
        this.modelMapper = modelMapper;
        this.imageRepository = imageRepository;
    }

    @Override
    public ImageEntity saveImage(MultipartFile file, String dni) throws IOException {
        ImageEntity image = new ImageEntity();
        byte[] fileBytes = file.getBytes();
        String encodedString = Base64.getEncoder().encodeToString(fileBytes);
        image.setImageContentType(file.getContentType());
        image.setNameImage(file.getOriginalFilename());
        image.setImageEncodedBytes(encodedString);
        image.setDni(dni);
        return imageRepository.save(image);
    }
}
