package com.pragma.hexagonal_arquitecture.image.domain.model;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

public class Image {
    private String id;
    private String imageContentType;
    private String nameImage;

    private String dni;

    private String imageEncodedBytes;

    public Image() {
    }

    public Image(String id, String imageContentType, String nameImage, String dni, String imageEncodedBytes) {
        this.id = id;
        this.imageContentType = imageContentType;
        this.nameImage = nameImage;
        this.dni = dni;
        this.imageEncodedBytes = imageEncodedBytes;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public String getNameImage() {
        return nameImage;
    }

    public void setNameImage(String nameImage) {
        this.nameImage = nameImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
}
