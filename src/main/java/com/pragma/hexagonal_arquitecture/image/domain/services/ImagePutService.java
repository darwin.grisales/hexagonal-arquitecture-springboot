package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.domain.model.Image;
import com.pragma.hexagonal_arquitecture.image.domain.ports.IImagePutService;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;

@Service
public class ImagePutService extends BaseService implements IImagePutService {
    private final ModelMapper modelMapper;
    public final MongoImageRepository imageRepository;

    public ImagePutService(ModelMapper modelMapper, MongoImageRepository imageRepository) {
        this.modelMapper = modelMapper;
        this.imageRepository = imageRepository;
    }

    @Override
    public ImageEntity updateImage(String id,
                                   MultipartFile file, String dni) throws IOException {
        byte[] fileBytes = file.getBytes();
        String encodedString = Base64.getEncoder().encodeToString(fileBytes);

        ImageEntity imageRequest = imageRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(ERROR_MESSAGE_TEMPLATE, id)));

        imageRequest.setNameImage(file.getOriginalFilename());
        imageRequest.setImageEncodedBytes(encodedString);
        imageRequest.setImageContentType(file.getContentType());
        imageRequest.setDni(dni);

        return imageRepository.save(imageRequest);
    }
}
