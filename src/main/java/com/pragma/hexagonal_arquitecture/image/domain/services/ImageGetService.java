package com.pragma.hexagonal_arquitecture.image.domain.services;

import com.pragma.hexagonal_arquitecture.image.domain.ports.IImageGetService;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.repository.MongoImageRepository;
import com.pragma.hexagonal_arquitecture.shared.domain.BaseService;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ImageGetService extends BaseService implements IImageGetService {
    private final MongoImageRepository imageRepository;

    public ImageGetService(MongoImageRepository mongoImageRepository) {
        super();
        this.imageRepository = mongoImageRepository;
    }

    @Override
    public List<ImageEntity> findAll() {
        return imageRepository.findAll();
    }

    @Override
    public ImageEntity findById(String id) {
        return imageRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(
                        String.format(ERROR_MESSAGE_TEMPLATE, id)));
    }

    @Override
    public List<ImageEntity> findAllByDni(String dni) {
        return imageRepository.findByDni(dni);
    }
}
