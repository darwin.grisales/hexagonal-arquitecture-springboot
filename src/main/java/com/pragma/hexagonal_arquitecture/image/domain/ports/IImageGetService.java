package com.pragma.hexagonal_arquitecture.image.domain.ports;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;

import java.util.List;

public interface IImageGetService {
    List<ImageEntity> findAll();
    ImageEntity findById(String id);
    List<ImageEntity> findAllByDni(String dni);
}
