package com.pragma.hexagonal_arquitecture.image.domain.ports;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;

public interface IImageDeleteService {
    ImageEntity delete(String id);
}
