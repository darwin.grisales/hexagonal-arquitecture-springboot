package com.pragma.hexagonal_arquitecture.image.domain.ports;

import com.pragma.hexagonal_arquitecture.image.infrastructure.persistence.entity.ImageEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IImagePutService {
    ImageEntity updateImage(String id, MultipartFile multipartFile, String dni) throws IOException;
}
