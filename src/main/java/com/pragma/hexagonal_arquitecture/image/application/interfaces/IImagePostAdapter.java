package com.pragma.hexagonal_arquitecture.image.application.interfaces;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface IImagePostAdapter {
    ImageDTO saveImage(MultipartFile file, String dni) throws IOException;
}
