package com.pragma.hexagonal_arquitecture.image.application.adapters;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImageDeleteAdapter;
import com.pragma.hexagonal_arquitecture.image.domain.ports.IImageDeleteService;
import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import org.springframework.stereotype.Service;

@Service
public class ImageDeleteAdapter extends BaseAdapter implements IImageDeleteAdapter {
    private final IImageDeleteService imageDeleteService;

    public ImageDeleteAdapter(IImageDeleteService imageDeleteService) {
        super();
        this.imageDeleteService = imageDeleteService;
    }

    @Override
    public ImageDTO deleteImage(String id) {
        if (id == null) {
            throw new BadRequestException("Param can not be null");
        }
        return modelMapper.map(imageDeleteService.delete(id), ImageDTO.class);
    }
}
