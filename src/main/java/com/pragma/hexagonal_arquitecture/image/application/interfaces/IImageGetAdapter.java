package com.pragma.hexagonal_arquitecture.image.application.interfaces;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;

import java.util.List;

public interface IImageGetAdapter {
    List<ImageDTO> findAllImages();
    ImageDTO findImageById(String id);
    List<ImageDTO> findAllImagesByDni(String dni);
}
