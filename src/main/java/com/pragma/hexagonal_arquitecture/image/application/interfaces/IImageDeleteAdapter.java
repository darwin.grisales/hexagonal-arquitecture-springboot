package com.pragma.hexagonal_arquitecture.image.application.interfaces;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;

public interface IImageDeleteAdapter {
    ImageDTO deleteImage(String id);
}
