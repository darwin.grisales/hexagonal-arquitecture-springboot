package com.pragma.hexagonal_arquitecture.image.application.adapters;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImagePutAdapter;
import com.pragma.hexagonal_arquitecture.image.domain.ports.IImagePutService;
import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImagePutAdapter extends BaseAdapter implements IImagePutAdapter {
    private final IImagePutService imagePutService;

    public ImagePutAdapter(IImagePutService imagePutService) {
        super();
        this.imagePutService = imagePutService;
    }

    @Override
    public ImageDTO updateImage(String id, MultipartFile file, String dni) throws IOException {
        if (id == null || id.equals("") || file.isEmpty() || dni == null || dni.equals("")) {
            throw new BadRequestException(
                    String.format("Param can not be null - File: %s and Dni: %s", file, dni));
        }
        return modelMapper.map(imagePutService.updateImage(id, file, dni), ImageDTO.class);
    }
}
