package com.pragma.hexagonal_arquitecture.image.application.adapters;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImagePostAdapter;
import com.pragma.hexagonal_arquitecture.image.domain.ports.IImagePostService;
import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImagePostAdapter extends BaseAdapter implements IImagePostAdapter {
    private final IImagePostService imagePostService;

    public ImagePostAdapter(IImagePostService imagePostService) {
        super();
        this.imagePostService = imagePostService;
    }

    @Override
    public ImageDTO saveImage(MultipartFile file, String dni) throws IOException {
        if (file.isEmpty() || dni == null || dni.equals("")) {
            throw new BadRequestException(
                    String.format("Param can not be null - File: %s and Dni: %s", file, dni));
        }
        return modelMapper.map(imagePostService.saveImage(file, dni), ImageDTO.class);
    }
}
