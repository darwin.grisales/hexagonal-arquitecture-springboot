package com.pragma.hexagonal_arquitecture.image.application.adapters;

import com.pragma.hexagonal_arquitecture.image.application.dto.ImageDTO;
import com.pragma.hexagonal_arquitecture.image.application.interfaces.IImageGetAdapter;
import com.pragma.hexagonal_arquitecture.image.domain.ports.IImageGetService;
import com.pragma.hexagonal_arquitecture.shared.application.BaseAdapter;
import com.pragma.hexagonal_arquitecture.shared.infrastructure.exceptions.BadRequestException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ImageGetAdapter extends BaseAdapter implements IImageGetAdapter {
    private final IImageGetService imageGetService;

    public ImageGetAdapter(IImageGetService imageGetService) {
        super();
        this.imageGetService = imageGetService;
    }

    @Override
    public List<ImageDTO> findAllImages() {
        return imageGetService.findAll()
                .stream()
                .map(image -> modelMapper.map(image, ImageDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ImageDTO findImageById(String id) {
        if (id == null || id.equals("")) {
            throw new BadRequestException("Param can not be null");
        }
        return modelMapper.map(imageGetService.findById(id), ImageDTO.class);
    }

    @Override
    public List<ImageDTO> findAllImagesByDni(String dni) {
        if (dni == null || dni.equals("")) {
            throw new BadRequestException("Param can not be null");
        }
        return imageGetService.findAllByDni(dni)
                .stream()
                .map(image -> modelMapper.map(image, ImageDTO.class))
                .collect(Collectors.toList());
    }
}
